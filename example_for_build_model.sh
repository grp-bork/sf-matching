#!/usr/bin/env bash
####################################################
# If you have a lot in-house spectra, you can add them to the spectra database, then re-build the model.
####################################################

PATH_SFMATCHING="Path to SF-Matching main program"
SPEC_DB="Spectra database file location" # There is an example file in the ./Data folder.

PATH_MODEL="Path to the model file"
PATH_TMP="Path to the temprate folder."

THREADS=24  # Or the max threads you have.
ION_TYPE=0  # For [M-H]-, 1 for [M+H]+
CAL_PART=30 # Every part can be run on one computer in parallel, increase the number if you want the model run on more computer at the same time.

"""
# This is one example for the parameter's setting
PATH_SFMATCHING='/home/yli/sf-matching/'
SPEC_DB='/home/yli/sf-matching/Data/database/spectral.db'
PATH_MODEL='/home/yli/sf-matching/Data/model/neg/'
PATH_TMP='/home/yli/sf-matching/Data/tmp'
THREADS=8
ION_TYPE=0
CAL_PART=3
#"""

cd ${PATH_SFMATCHING}/SearchWithModel/
python setup.py build_ext --inplace
cd -

python ${PATH_SFMATCHING}/SearchWithModel/10_ExtractDatabaseInformation.py -d \
    -db ${SPEC_DB} \
    -out_path ${PATH_TMP} \
    -threads ${THREADS} \
    -extract_cutoff 5 \
    -ion ${ION_TYPE}

for NUM in $(seq 0 1 $((${CAL_PART} - 1))); do
    # This program can be runned in multiple computers at the same time.
    python ${PATH_SFMATCHING}/SearchWithModel/20_FitPeakModels.py \
        -in_path ${PATH_TMP}/ \
        -model ${PATH_TMP}/ \
        -cal_part ${NUM} \
        -total_part ${CAL_PART} \
        -threads ${THREADS} \
        -ion ${ION_TYPE}
done

python ${PATH_SFMATCHING}/SearchWithModel/21_MergePeakModels.py \
    -model ${PATH_TMP}/ \
    -total_part ${CAL_PART} \
    -ion ${ION_TYPE}

mkdir -p ${PATH_MODEL}
mv ${PATH_TMP}/peak_model_merged.data ${PATH_MODEL}/
mv ${PATH_TMP}/peak_model_merged.info ${PATH_MODEL}/
