#!/usr/bin/env bash
# For building models
PATH_SFMATCHING="Path to SF-Matching main program"
FILE_SPEC="Spectra file, can be mgf / mzML / mzXML file format."
PATH_LIBRARY="Folder of library file, should contain molecular_spectra_neg.db, spectra_data_neg.bin or molecular_spectra_pos.db, spectra_data_pos.bin"
FILE_OUT="The output file, csv format."
ION_TYPE=0  # For [M-H]-, 1 for [M+H]+

"""
#This is one example for the parameter's setting
PATH_SFMATCHING='/home/yli/sf-matching/'
FILE_SPEC='/home/yli/sf-matching/Data/examples/test-001.mgf'
PATH_LIBRARY='/home/yli/sf-matching/Data/library/'
FILE_OUT='/home/yli/sf-matching/Data/examples/result-001_library.csv'
#"""

python ${PATH_SFMATCHING}/SearchWithPreCalculatedLibrary/DatabaseSearching.py \
    -spectra ${FILE_SPEC} \
    -db_path ${PATH_LIBRARY} \
    -out ${FILE_OUT} \
    -ion ${ION_TYPE}
