#!/usr/bin/env python
import argparse
import os
from argparse import ArgumentParser

from Main.Parameter import Parameter


def check_folder(path):
    path_name = os.path.abspath(path)
    if not (os.path.isdir(path_name) and os.path.exists(path_name)):
        try:
            os.makedirs(path_name)
        except:
            pass
    result = path_name + '/'
    return result


class MainArguments(ArgumentParser):
    def __init__(self, *args, **kwargs):
        super(MainArguments, self).__init__(add_help=True, *args, **kwargs)
        self.formatter_class = argparse.RawTextHelpFormatter

        self.add_argument('-ion', required=False, type=int, dest='ion', default=0,
                          metavar='0/1/2 ...', help='Set the ionization method. default=0\n'
                                                    '0: [M-H]-\n'
                                                    '1: [M+H]+')

        self.add_argument('-ms2_ppm', required=False, type=float, dest='ms2_ppm', default='50.0',
                          help='Set the ppm for MS2. default=50.0')

        # The following deals with input and output files.
        self.add_argument('-db', required=False, type=str, dest='database', default='',
                          metavar='Metabolite database name', help='Set the metabolite database name')

        self.add_argument('-out_file', required=False, type=str, dest='output_file', default='',
                          metavar='', help='')

        self.add_argument('-out_path', required=False, type=str, dest='output_path', default='',
                          metavar='folder name', help='Set the output path')

        self.add_argument('-in_file', required=False, type=str, dest='input_file', default='',
                          metavar='', help='')

        self.add_argument('-in_path', required=False, type=str, dest='input_path', default='',
                          metavar='', help='')

        self.add_argument('-model', required=False, type=str, dest='model', default='',
                          help='Set model pathname')

        self.add_argument('-spectra', required=False, type=str, dest='spectra', default='',
                          help='Set spectra pathname')

        # self.add_argument('-model_fp', required=False, type=str, dest='model_fingerprint_filename', default='',
        #                  help='Set model filename')

        self.add_argument('-max_peaks', required=False, type=str, dest='max_peaks', default='100',
                          help='max peaks to calculate.')

        self.add_argument('-fp_type_bits', required=False, type=str, dest='fp_type_bits',
                          default='MolToRDKitFP:8191',
                          metavar='MolToHashedAtomPairFP:4093, MolToHashedTopologicalTorsionFP:4039, ...',
                          help='type: MolToHashedAtomPairFP, MolToRDKitFP, MolToMorganFP,'
                               ' MolToHashedTopologicalTorsionFP, MolToAvalonFP\n'
                               'bits: 4093, 8191, 8192, 16381\n'
                               'examples: MolToHashedAtomPairFP:4093,MolToHashedTopologicalTorsionFP:4039')

        # self.add_argument('-model_type', required=False, type=int, dest='model_type', default=7,
        #                  metavar='', help='')

        # Other information
        self.add_argument("-d", dest='debug', default=False, action='store_true')

        self.add_argument('-threads', required=False, type=int, dest='threads', default=7,
                          metavar='number', help='The threads used to run the programme.')

        self.add_argument('-no_iso', default=False, dest='no_iso', action='store_true',
                          help='No isotopic peak in the spectrum.')

        # self.add_argument('-use_mmff', required=False, type=int, dest='use_mmff', default=1,
        #                  metavar='number', help='Use MMFF to translate mol or not.')

        # self.add_argument('-use_hdf5', dest='use_hdf5', default=False, action='store_true')

        self.add_argument('-use_saved_spectra', dest='use_saved_spectra', default=False, action='store_true')
        # self.add_argument('-use_saved_values', dest='use_saved_values', default=False, action='store_true')
        # self.add_argument('-normalize', dest='normalize', default=False, action='store_true')
        self.add_argument('-casmi2017', dest='casmi2017', default=False, action='store_true')
        self.add_argument('-not_guess_adduct', dest='not_guess_adduct', default=False, action='store_true')
        pass

    def parse_args(self, args=None, namespace=None):
        args_result = super(MainArguments, self).parse_args(args, namespace=Parameter)

        if Parameter.ion == 0:
            Parameter.addition = '-H'
            Parameter.ion_charge = 1
        elif Parameter.ion == 1:
            Parameter.addition = '+H'
            Parameter.ion_charge = 0

        # The following deals with input and output files.
        if Parameter.output_path:
            Parameter.output_path = check_folder(Parameter.output_path)
        if Parameter.input_path:
            Parameter.input_path = check_folder(Parameter.input_path)

        fp_type_bits = Parameter.fp_type_bits.split(',')
        Parameter.fp_type_bits = []
        for type_bits in fp_type_bits:
            fp_type, bits = type_bits.split(':')
            Parameter.fp_type_bits.append((str(fp_type.strip()), int(bits)))

        return args_result

    pass
