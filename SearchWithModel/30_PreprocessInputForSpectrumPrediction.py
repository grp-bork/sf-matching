#!/usr/bin/env python

from Main.MainArguments import MainArguments
from Main.Parameter import Parameter
from SpectraDatabase.SpectraDBAdaptor import SpectraDBAdaptor

"""
Running parameters:
-in_file
/data/model/inchi.txt
-db
/data/model/molecular_spectra_neg.db
"""


def main():
    deal_with_arguments()
    db = SpectraDBAdaptor(Parameter.database, Parameter.spectra)
    db.filter_molecules(Parameter.input_file)
    batch_num = db.re_cluster_candidate_molecules(Parameter.batch_size)
    print(
        "Total have {} parts. Please run from -batch_num=0 to -batch_num={} respectively in next step.".format(
            batch_num, batch_num))
    pass


def deal_with_arguments():
    parser = MainArguments(description='')

    parser.add_argument('-batch_size', required=False, type=int, dest='batch_size',
                        default=10000, help='Set batch size of the molecules to be analyzed.')

    parser.parse_args()


if __name__ == '__main__':
    main()
