#!/usr/bin/env python
import argparse
from argparse import ArgumentParser

from .Parameter import Parameter


class Arguments(ArgumentParser):
    def __init__(self, *args, **kwargs):
        super(Arguments, self).__init__(add_help=True, *args, **kwargs)
        self.formatter_class = argparse.RawTextHelpFormatter

        self.add_argument('-db_path', required=True, type=str, dest='db_path', default='',
                          metavar='file_path', help='The in silico database file path.\n')

        self.add_argument('-spectra', required=True, type=str, dest='spectrum', default='',
                          metavar='filename',
                          help='The spectra file, should be in mgf/mzML/mzXML file format.\n')

        self.add_argument('-out', required=True, type=str, dest='output_file', default='',
                          metavar='filename', help='The output filename.\n')

        self.add_argument('-ion', required=False, type=int, dest='ion', default=0,
                          metavar='0 or 1', help='Set the ionization method. default=0\n'
                                                 '0: [M-H]-\n'
                                                 '1: [M+H]+\n')

        self.add_argument('-all_result', default=False, dest='all_result', action='store_true',
                          help='Output all result, if not set, only output the highest hit. '
                               'If set, will output all molecules. default=False.\n')
        """
        self.add_argument('-mol', required=False, type=str, dest='mol',
                          default='', help='Set candidate molecules file information.')
        
        """
        self.add_argument('-ms1_ppm', required=False, type=float, dest='ms1_ppm', default='10.0',
                          metavar='ms1_ppm', help='Set the ppm for MS1, default=10.\n')

        self.add_argument('-ms2_ppm', required=False, type=float, dest='ms2_ppm', default='50.0',
                          metavar='ms2_ppm', help='Set the ppm for MS2, default=50.\n')

        # The following deals with input and output files.
        self.add_argument('-max_peaks', required=False, type=str, dest='max_peaks', default='100',
                          metavar='number', help='max peaks to calculate, default=100.\n')

        # Other information
        self.add_argument('-threads', required=False, type=int, dest='threads', default=8,
                          metavar='number', help='The threads used to run the programme, default=8.\n')

        # self.add_argument('-no_iso', default=False, dest='no_iso', action='store_true',
        #                  help='No isotopic peak in the spectrum.\n')

        pass

    def parse_args(self, args=None, namespace=None):
        args_result = super(Arguments, self).parse_args(args, namespace=Parameter)

        if Parameter.ion == 0:
            Parameter.addition = '-H'
            Parameter.ion_charge = 1
            Parameter.database = Parameter.db_path + "/molecular_spectra_neg.db"
            Parameter.model = Parameter.db_path + "/spectra_data_neg.bin"
        elif Parameter.ion == 1:
            Parameter.addition = '+H'
            Parameter.ion_charge = 0
            Parameter.database = Parameter.db_path + "/molecular_spectra_pos.db"
            Parameter.model = Parameter.db_path + "/spectra_data_pos.bin"

        return args_result

    pass
