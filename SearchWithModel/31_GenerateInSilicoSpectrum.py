#!/usr/bin/env python
from Main.MainArguments import MainArguments
from Main.Parameter import Parameter
from Model.ModelBuilder import ModelBuilder
from SpectraDatabase import SpectraPredictor
from SpectraDatabase.SpectraDBAdaptor import SpectraDBAdaptor
from Utils import FilenameGenerator

"""
Running parameters:
-db /data/model/molecular_spectra_neg.db
-spectra /d/test/tmp/ 
-model /d/test/model/neg/
-batch_num 0 
-threads 8
-ion 0
"""


def main():
    deal_with_arguments()

    print('Start Reading mol information.')
    db = SpectraDBAdaptor(Parameter.database, Parameter.spectra)
    molecules_to_be_processed = db.get_to_be_processed_molecules(Parameter.batch_num)

    if not molecules_to_be_processed:
        print('No molecule to be analyzed.')
        return

    print('Start generate in silico spectra.')
    model = ModelBuilder.load_from_file(FilenameGenerator.merged_peak_model_info(Parameter.model),
                                        load_training_data=False)

    all_right_mol_id, all_fingerprints, all_wanted_ff = \
        SpectraPredictor.predict_mass(model, molecules_to_be_processed, SpectraPredictor.generate_spectra_path())

    SpectraPredictor.predict_p(model, all_fingerprints, all_wanted_ff, SpectraPredictor.generate_spectra_path())

    SpectraPredictor.generate_spectra(all_right_mol_id, SpectraPredictor.generate_spectra_path())
    print('Done.')
    pass


def deal_with_arguments():
    parser = MainArguments(description='')

    parser.add_argument('-batch_num', required=False, type=int, dest='batch_num',
                        default=0, help='Set batch number of the molecules to be analyzed.')

    parser.parse_args()


if __name__ == '__main__':
    main()
