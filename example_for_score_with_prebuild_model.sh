#!/usr/bin/env bash
# For building models
PATH_SFMATCHING="Path to SF-Matching main program"
PATH_MODEL="Folder of model file"

FILE_MGF="Mgf file."
FILE_INCHI="InChI file, one line per molecule."
FILE_OUT="Result file."

THREADS=24 # Or the max threads you have.
ION_TYPE=0 # For [M-H]-, 1 for [M+H]+

"""
# This is one example for the parameter's setting
PATH_SFMATCHING='/home/yli/sf-matching/'
FILE_MGF='/home/yli/sf-matching/Data/examples/test-001.mgf'
FILE_INCHI='/home/yli/sf-matching/Data/examples/test-001.txt'
PATH_MODEL='/home/yli/sf-matching/Data/model/neg/'
FILE_OUT='/home/yli/sf-matching/Data/examples/result-001_model.txt'
#"""

python ${PATH_SFMATCHING}/SearchWithModel/ScoreWithPreBuildModel.py \
    -spectrum ${FILE_MGF} \
    -mol ${FILE_INCHI} \
    -model ${PATH_MODEL} \
    -out_file ${FILE_OUT} \
    -ion ${ION_TYPE} \
    -threads ${THREADS}
