# Authors: Gilles Louppe <g.louppe@gmail.com>
#          Peter Prettenhofer <peter.prettenhofer@gmail.com>
#          Brian Holt <bdholt1@gmail.com>
#          Joel Nothman <joel.nothman@gmail.com>
#          Arnaud Joly <arnaud.v.joly@gmail.com>
#          Jacob Schreiber <jmschreiber91@gmail.com>
#          Nelson Liu <nelson@nelsonliu.me>
#
# License: BSD 3 clause

# See _tree.pyx for details.
cimport numpy as np
import numpy as np

ctypedef np.npy_float32 DTYPE_t  # Type of X
ctypedef np.npy_float64 DOUBLE_t  # Type of y, sample_weight
ctypedef np.npy_int16 SIZE_t  # Type for indices and counters
ctypedef np.npy_int16 INT16_t
ctypedef np.npy_uint16 UINT16_t
ctypedef np.npy_int32 INT32_t  # Signed 32 bit integer
ctypedef np.npy_uint32 UINT32_t  # Unsigned 32 bit integer
ctypedef np.npy_int64 INT64_t  # Signed 32 bit integer

cdef struct Node:
    # Base storage structure for the nodes in a Tree object
    SIZE_t left_child  # id of the left child of the node
    SIZE_t right_child  # id of the right child of the node
    UINT16_t feature  # Feature used for splitting the node

ctypedef Node*realloc_ptr
cdef realloc_ptr safe_realloc_node(realloc_ptr*p, size_t nelems) nogil except *

cdef class Tree:
    cdef public SIZE_t node_count  # Counter for node IDs
    cdef Node*nodes  # Array of nodes

    cpdef np.ndarray apply(self, object X,
                           object tree_nodes, object nodes_len,
                           object values_len)
    cpdef np.ndarray predict(self, object X)
    cdef np.ndarray _get_node_ndarray(self)
