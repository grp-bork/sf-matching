from . import mgfFile
from . import mzMLFile
from . import mzXMLFile


def get_spectrum_info(filename):
    if filename.endswith(".mgf"):
        return mgfFile.read(filename)
    elif filename.lower().endswith(".mzml"):
        return mzMLFile.read(filename, wanted_ms_level={2})
    elif filename.lower().endswith(".mzxml"):
        return mzXMLFile.read(filename, wanted_ms_level={2})
    pass
