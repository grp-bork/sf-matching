class ParameterClass(object):
    def __init__(self):
        self.ion_charge = 1  # 0: positive, 1:negative
        self.addition = '-H'

        self.ms1_ppm = 50.0
        self.ms2_ppm = 50.0
        self.min_detect_mass = 40.0  # The minimum detected mass.
        self.detect_threshold = 0.01
        self.ms2_ppm_step = 50.0
        self.ms2_max_delta = 0.1
        self.remain_peaks = 100  # The max peak number in a spectrum.

        self.database = ''
        self.output_path = ''
        self.output_file = ''
        self.input_file = ''
        self.input_path = ''

        self.model = ''
        self.model_fingerprint_filename = ''
        self.formula_extract_types = ['remain', 'lost_directly']

        self.total_cv_part = 10
        self.debug = False  # Is debug mode or not
        self.normalize = False
        self.threads = 7  # The maximum used threads.
        self.use_mmff = 1
        self.use_hdf5 = 0
        self.use_saved_spectra = 0


Parameter = ParameterClass()
