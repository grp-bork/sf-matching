class FileMSP(object):
    def __init__(self, filename, filemode='w'):
        self._file = open(filename, filemode)
        pass

    def write_spectrum_to_file(self, spectrum_info):
        output_string = ["""NAME: {name}
PRECURSORMZ: {ion_mass}
PRECURSORTYPE: {ion_type}
MW: {mass}
Num Peaks: {peak_num}
""".format(name=spectrum_info['name'],
           ion_mass=spectrum_info['ion_mass'],
           ion_type=spectrum_info['ion_type'],
           mass=spectrum_info['mass'],
           peak_num=spectrum_info['spectrum'].shape[0])]
        for peak in spectrum_info['spectrum']:
            output_string.append('{mass}\t{intensity}\n'.format(mass=peak[0], intensity=peak[1]))
        output_string.append('\n')
        self._file.write(''.join(output_string))
        pass
