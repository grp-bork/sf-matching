from Main.MainArguments import check_folder


def three_score_merge_model(path):
    filename_out = '{}/three_score_merge.model'.format(path)
    return filename_out


def two_score_merge_model(path):
    filename_out = '{}/two_score_merge.model'.format(path)
    return filename_out


def cv_score_fp_model(path, cv_num):
    filename_out = '{}/cv_{}/score_fp.data'.format(path, cv_num)
    return filename_out


def cv_score_lda_model(path, cv_num):
    filename_out = '{}/cv_{}/score_lda.data'.format(path, cv_num)
    return filename_out


def cv_score_peak_model(path, cv_num, cal_part):
    filename_out = '{}/cv_{}/score_peak_{}.data'.format(path, cv_num, cal_part)
    return filename_out


def cv_score_frag_model(path, cv_num, cal_part):
    filename_out = '{}/cv_{}/score_frag_{}.data'.format(path, cv_num, cal_part)
    return filename_out


def annotated_bg_mol(path, spec_id, cv_num):
    id_folder = spec_id % 100
    path_output = '{}/cv_{}/mol/{}/'.format(path, cv_num, id_folder)
    check_folder(path_output)
    output_filename = '{}/spec_{}.info'.format(path_output, spec_id)
    return output_filename


def annotated_bg_mol_path(path, cv_num):
    all_path = []
    for i in range(100):
        path_output = '{}/cv_{}/mol/{}/'.format(path, cv_num, i)
        all_path.append(path_output)

    return all_path


def annotated_mol(path, spec_id):
    id_folder = spec_id % 100
    path_output = '{}/{}'.format(path, id_folder)
    check_folder(path_output)
    output_filename = '{}/spec_{}.info'.format(path_output, spec_id)
    return output_filename


def annotated_mol_all(path):
    output_filename = '{}/all_spec.info'.format(path)
    return output_filename


def feature_data(path):
    output_filename = '{}feature.model'.format(path)
    return output_filename


def test_data(path, cv_num):
    output_filename = '{}test_{}.model'.format(path, cv_num)
    return output_filename


def abstract_feature_data(path):
    output_filename = '{}abstract_feature.model'.format(path)
    return output_filename


def part_feature_values_data(path, part, extract_type):
    output_filename = '{}/spectra_{}_{}.npy'.format(path, part, extract_type)
    return output_filename


def feature_values_data(path, extract_type):
    output_filename = '{}/spectra_{}.npy'.format(path, extract_type)
    return output_filename


def feature_values_meta_data(path):
    output_filename = '{}/spectra.metadata'.format(path)
    return output_filename


def model_data(path, extract_type, feature, should_check_folder=True):
    if type(feature) is tuple:
        feature_0, feature_2 = feature
        feature_1 = hash(str(feature_2)) % 100
    else:
        feature_2 = str(feature)
        feature_0 = hash(feature_2) % 10
        feature_1 = hash(feature_2) % 100

    if should_check_folder:
        try:
            check_folder(path)
            check_folder('{}/{}'.format(path, extract_type))
            check_folder('{}/{}/{}'.format(path, extract_type, feature_0))
            check_folder('{}/{}/{}/{}'.format(path, extract_type, feature_0, feature_1))
        except:
            pass

    output_filename = '{}/{}/{}/{}/{}.model'.format(path, extract_type, feature_0, feature_1, feature_2)
    return output_filename


def peak_model_data(path):
    output_folder = '{}/peak_model'.format(path)
    check_folder(output_folder)
    return output_folder


def merged_peak_model_info(path):
    output_folder = '{}/peak_model_merged.info'.format(path)
    return output_folder


def merged_peak_model_data(path):
    output_folder = '{}/peak_model_merged.data'.format(path)
    return output_folder


def fp_model_data(path):
    output_folder = '{}/fp_model'.format(path)
    check_folder(output_folder)
    return output_folder


def lda_model_data(path):
    output_folder = '{}/lda_model'.format(path)
    check_folder(output_folder)
    return output_folder


def spectrum_hdf5_data(path):
    output_filename = '{}spectra.hdf5'.format(path)
    return output_filename


def in_silico_spectra(filename):
    if filename.endswith('/'):
        filename = filename[:-1]
    return '{}.bin'.format(filename)


def in_silico_spectra_data(pathname):
    check_folder(pathname)
    return '{}/spec_data.bin'.format(pathname)


def in_silico_spectra_info(pathname):
    check_folder(pathname)
    return '{}/spec_info.bin'.format(pathname)


def in_silico_spectra_error_mol(pathname):
    check_folder(pathname)
    return '{}/spec.error.txt'.format(pathname)


def in_silico_spectra_fingerprint_data(pathname):
    check_folder(pathname)
    return '{}/spec_fp_data.bin'.format(pathname)


def in_silico_spectra_peak_mass_data(pathname):
    check_folder(pathname)
    return '{}/spec_peak_mass_data.bin'.format(pathname)


def in_silico_spectra_peak_mass_info(pathname):
    check_folder(pathname)
    return '{}/spec_peak_mass_info.bin'.format(pathname)


def in_silico_spectra_peak_p_data(pathname):
    check_folder(pathname)
    return '{}/spec_peak_p_data.bin'.format(pathname)


def in_silico_spectra_peak_p_info(pathname):
    check_folder(pathname)
    return '{}/spec_peak_p_info.bin'.format(pathname)
