import numpy as np

# To save memory usage, the max accepted atom number is 255.
numpy_formula_format = np.int8
formula_max_atom_num = 2 ** 7 - 1
formula_max_atom_bit = 8

extract_type_place = {'frag_formula': 0, 'root_loss': 1}
