#!/usr/bin/env python

import sklearn.ensemble

from Main.MainArguments import MainArguments
from Main.Parameter import Parameter
from Model.EstimatorForPeakModels import EstimatorForPeakModels
from Model.ModelBuilder import ModelBuilder
from Utils import FilenameGenerator

"""
-in_path /d/test/tmp/
-model /d/test/tmp/
-cal_part 0
-total_part 2
-threads 8
-ion 0
"""


def main():
    deal_with_arguments()
    training_data = ModelBuilder.load_from_file(FilenameGenerator.feature_data(Parameter.model))

    print('Start.')

    estimator = EstimatorForPeakModels(sklearn.ensemble.ExtraTreesClassifier(n_estimators=100),
                                       FilenameGenerator.peak_model_data(Parameter.model))
    training_data.fit_peak_model(estimator)
    print('Done.')
    pass


def deal_with_arguments():
    parser = MainArguments(description='')

    parser.add_argument('-cal_part', required=False, type=int, dest='cal_part', default=0,
                        metavar='', help='')

    parser.add_argument('-total_part', required=False, type=int, dest='total_part', default=1,
                        metavar='', help='')

    parser.parse_args()


if __name__ == '__main__':
    main()
