#!/usr/bin/env python
from distutils.core import setup, Extension

import numpy as np
from Cython.Build import cythonize

# python setup.py build_ext --inplace

extensions = [Extension("Model/*/*",
                        ["Model/*/*.pyx"],
                        include_dirs=[np.get_include()])]

setup(name='sf-matching',
      version='1.0',
      description='',
      author='Yuanyue Li',
      author_email='yli@embl.de',
      url='',
      packages=[],
      ext_modules=cythonize(extensions),
      install_requires=[
          'numpy',
          'scipy',
          'pandas',
          'scikit-learn',
          'joblib',
          'rdkit'
      ]
      )
