#!/usr/bin/env bash
# Pre-calculate spectra from trained moedel.

PATH_SFMATCHING="Path to SF-Matching main program"
PATH_MODEL="Path to the model file"
PATH_TMP="Path to the temprate folder."

FILE_INCHI="File to the InChI of molecules, in txt format, one line for one molecule."
FILE_DB="molecular_spectra_neg.db" # Or molecular_spectra_pos.db, file to database.
FILE_OUT="spectra_data_neg.bin"    # Or spectra_data_pos.bin, the output filename.

THREADS=24  # Or the max threads you have.
ION_TYPE=0  # For [M-H]-, 1 for [M+H]+
CAL_PART=30 # Every part can be run on one computer in parallel, increase the number if you want the model run on more computer at the same time.

"""
# This is one example for the parameter's setting
PATH_SFMATCHING='/home/yli/sf-matching/'
PATH_MODEL='/home/yli/sf-matching/Data/model/neg/'
PATH_TMP='/home/yli/sf-matching/Data/tmp'
FILE_INCHI='/home/yli/sf-matching/Data/examples/test-001.txt'
FILE_DB='/home/yli/sf-matching/Data/library/molecular_spectra_neg.db'
FILE_OUT='/home/yli/sf-matching/Data/library/spectra_data_neg.bin'
THREADS=8
ION_TYPE=0
#"""

rm ${FILE_DB}
sqlite3 ${FILE_DB} <${PATH_SFMATCHING}/SearchWithModel/sql.txt

python ${PATH_SFMATCHING}/SearchWithModel/30_PreprocessInputForSpectrumPrediction.py \
    -in_file ${FILE_INCHI} \
    -db ${FILE_DB} \
    -threads ${THREADS} \
    -ion ${ION_TYPE}

CAL_PART=1 #The command above will tell you this value.

for NUM in $(seq 0 1 $((${CAL_PART} - 1))); do
    python ${PATH_SFMATCHING}/SearchWithModel/31_GenerateInSilicoSpectrum.py \
        -db ${FILE_DB} \
        -spectra ${PATH_TMP} \
        -model ${PATH_MODEL} \
        -batch_num ${NUM} \
        -threads ${THREADS} \
        -ion ${ION_TYPE}
done

python ${PATH_SFMATCHING}/SearchWithModel/32_SaveSpectrum.py \
    -db ${FILE_DB} \
    -spectra ${PATH_TMP} \
    -model ${PATH_MODEL} \
    -out_file ${FILE_OUT} \
    -threads ${THREADS} \
    -ion ${ION_TYPE}
