import numpy as np


class Spectrum(object):
    def __init__(self,
                 experiment,
                 experiment_spec_id,
                 data,
                 scan_num=-1,
                 retention_time=-1.,
                 ms_level=0,
                 precursor_mz=0.,
                 precursor_charge=0,
                 *args, **kwargs):
        # super(spectrum, self).__init__(*args, **kwargs)

        self.data = data  # The data of the spectrum
        self.experiment_idx = experiment_spec_id  # The index of spectrum in the experiment
        self.raw_scan_num = scan_num  # The raw scan number in origin input file.
        self.retention_time = retention_time  # The retention time of the spectrum.
        self.ms_level = ms_level  # The ms level of the spectrum.
        self.precursor_mz = precursor_mz  # The  m/z of the precursor of the spectrum.
        self.precursor_charge = precursor_charge  # The charge of the precursor ion
        if "annotation" in kwargs:
            self.annotation = kwargs["annotation"]
        else:
            self.annotation = None

        # The link to the experiment which contain this spectrum.
        self._experiment = experiment
        self._index = None

    def __getitem__(self, item):
        return self.data[item]

    def __len__(self):
        if self.data is not None:
            return len(self.data)
        else:
            return 0

    def __repr__(self):
        return str(self.raw_scan_num) + ':' + str(self.retention_time)

    def get_index_between_mz(self, mz_low, mz_high):
        idx_low = self.get_lower_index_of_mz(mz_low)
        idx_high = self.get_higher_index_of_mz(mz_high)
        return idx_low, idx_high

    def get_lower_index_of_mz(self, mz):
        idx = np.searchsorted(self.data[0], mz, side="left")
        return idx

    def get_higher_index_of_mz(self, mz):
        idx = np.searchsorted(self.data[0], mz, side="right")
        return idx

    def find_next_max_intensity_peak_index(self, cur_peak_index, max_searched_peak_index=None):

        pass
