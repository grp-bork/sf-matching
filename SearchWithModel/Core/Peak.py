class Peak(dict):
    def __init__(self, mass=0.0, intensity=0.0):
        super(Peak, self).__init__()
        self['mass'] = mass
        self['intensity'] = intensity

    def __repr__(self):
        return str(self['mass']) + ':' + str(self['intensity'])

    pass
