#!/usr/bin/env python
from Main.MainArguments import MainArguments
from Main.Parameter import Parameter
from SpectraDatabase import DatabaseCompresser
from SpectraDatabase import SpectraPredictor
from SpectraDatabase.SpectraDBAdaptor import SpectraDBAdaptor

"""
Running parameters:
-db /data/model/molecular_spectra_neg.db
-spectra /d/test/tmp/
-out /d/test/library/spectra_data_neg.bin
"""


def main():
    deal_with_arguments()
    file_tmp_spec = Parameter.spectra + '/spectra.bin'

    db = SpectraDBAdaptor(Parameter.database, file_tmp_spec)
    max_batch_num = db.get_max_cluster_num()

    fo = open(file_tmp_spec, 'wb')
    for batch_num in range(max_batch_num + 1):
        try:
            spec_info, fi = SpectraPredictor.get_spectra(SpectraPredictor.generate_spectra_path(batch_num))
        except Exception as e:
            print('Batch number {} error: {}'.format(batch_num, e))
            continue

        # Write to database
        db.storage_spectra(spec_info, fi, fo)

    print('Start compressing sepctra.')

    DatabaseCompresser.compress_spectra(db,
                                        file_tmp_spec,
                                        Parameter.output_file,
                                        select_peak_num=1000)

    print('Done.')

    pass


def deal_with_arguments():
    parser = MainArguments(description='')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
